ReadPolicyNumber:

There is an insurance policy company that is going multi-national. And now it faces a very unique problem. It seems that each country's customers read out their policy number differently.
For example, in Japan the policy number is 9 digits , say 243691222 and format of reading is 2-2-5. So this is how it looks : 24-36-91222, and this is what it should be read : two four three six nine one triple two
And if the format was 2-5-2 (number now looks like 24-36912-22) , then it would be read as : two four three six nine one two double two

You are provided with a list of policy numbers and the division formats. You need to output the correct way to read those numbers.

Rules:

Single numbers just read them individually.

2 continuous numbers use the word double

3 continuous numbers use the word triple

4 continuous numbers use the word quadruple

5 continuous numbers use the word quintuple

6 continuous numbers use the word sextuple

7 continuous numbers use the word septuple

8 continuous numbers use the word octuple

9 continuous numbers use the word nonuple

10 continuous numbers use the word decuple

In case of more than 10 continuous numbers, just read them all separately.

Input

The first line of the input contains the number of test cases, T. 
T lines follow. 
Each line equals a test case. It contains a policy number N and the division format F. Division format is one or more positive integers separated by dashes (-), without any leading zeros, and their sum always equals the number of digits in the policy number.

Output

For each test case, output one line containing "Case #x: y", where x is the test case number starting from 1 and y is the sentence in English where words are space separated.

Limits

1 <= T <= 100.

1 <= length of N <= 100.

Example

Input:
3
15092233555 4-3-4
13012288555 2-4-5
19993 3-2


Output 
Case #1: one five zero nine double two three three triple five
Case #2: one three zero one double two double eight triple five
Case #3: one double nine nine three
