import java.util.Hashtable;
import java.util.HashMap;
import java.util.Map;
import java.io.*;
import java.util.Arrays;

public class ReadPolicyNumber {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ReadPhoneNumber readPhoneNumber = new ReadPhoneNumber();
		readPhoneNumber.Solve();
	}

}

class ReadPhoneNumber {
	public static Hashtable<Integer, String> RepetetionSpellings = new Hashtable<Integer, String>();
	static {
		RepetetionSpellings.put(2, "double");
		RepetetionSpellings.put(3, "triple");
		RepetetionSpellings.put(4, "quadruple");
		RepetetionSpellings.put(5, "quintuple");
		RepetetionSpellings.put(6, "sextuple");
		RepetetionSpellings.put(7, "septuple");
		RepetetionSpellings.put(8, "octuple");
		RepetetionSpellings.put(9, "nonuple");
		RepetetionSpellings.put(10, "decuple");
	}

	public static Hashtable<Character, String> DigitSpellings = new Hashtable<Character, String>();
    static {
    	DigitSpellings.put('0', "zero");
    	DigitSpellings.put('1', "one");
    	DigitSpellings.put('2', "two");
    	DigitSpellings.put('3', "three");
    	DigitSpellings.put('4', "four");
    	DigitSpellings.put('5', "five");
    	DigitSpellings.put('6', "six");
    	DigitSpellings.put('7', "seven");
    	DigitSpellings.put('8', "eight");
    	DigitSpellings.put('9', "nine");
    };
    
    public void Solve() throws IOException
    {
    	ManageMyIO manageMyIO = new ManageMyIO();
    	int testCaseCount = Integer.parseInt(manageMyIO.Read());
    	for (int i = 0; i < testCaseCount; i++)
        {
            ReadPhoneNumberTestCase testCase = new ReadPhoneNumberTestCase(manageMyIO.Read());
            String result = String.format("Case #%d: %s\n", i + 1, testCase.Solve());
            manageMyIO.write(result);
        }
    }
}

class ReadPhoneNumberTestCase
{
    private String _fullNumber;
    private String[] _componentLengths;

    public ReadPhoneNumberTestCase(String line)
    {
        String[] info = line.split(" ");
        _fullNumber = info[0];
        _componentLengths = info[1].split("-");
    }

    public String Solve()
    {
        int lastIndex = 0;
        StringBuilder stringBuilder = new StringBuilder(); 
        for (String s : _componentLengths)
        {
            int length = Integer.parseInt(s);
            
            String numberToSpell = _fullNumber.substring(lastIndex, lastIndex+length);
            stringBuilder.append(SpellComponent(numberToSpell));
            stringBuilder.append(" ");
            lastIndex += length;
        }
        return stringBuilder.toString().trim();
    }

    private String SpellComponent(String component)
    {
        StringBuilder stringBuilder = new StringBuilder();
        boolean ForceIndividualWriteOut = false;
        char forcingIndividualWriteOutForCharacter = ' ';
        boolean skipRepetetionCheck = false;
        String s;

        for (int i = 0; i < component.length(); i++)
        {
            char c = component.charAt(i);
            skipRepetetionCheck = false;

            if ((ForceIndividualWriteOut == true) && (forcingIndividualWriteOutForCharacter == c))
            {
                skipRepetetionCheck = true;
            }
            else
            {
                ForceIndividualWriteOut = false;
                forcingIndividualWriteOutForCharacter = ' ';
            }

            if ((skipRepetetionCheck == false) && ((i + 1) < component.length()) && (component.charAt(i + 1) == c))
            {
                // we are in repetations
                int j = i; // backup, in case the count exceeds 10
                int count = 0;
                while ((i < component.length()) && (component.charAt(i) == c))
                {
                    count++;
                    i++;
                }

                i--;

                if (count <= 10)
                {
                    //stringBuilder.AppendFormat("{0} ", ReadPhoneNumber.RepetetionSpellings.get(count));
                	s = String.format("%s ", ReadPhoneNumber.RepetetionSpellings.get(count));
                	stringBuilder.append(s);
                }
                else
                {
                    // in this case we ensure that till all the c charatcters pass away, we do not try and spell the repetetions
                    ForceIndividualWriteOut = true;
                    forcingIndividualWriteOutForCharacter = c;
                    i = j;
                }
            }

            //String spelling = ReadPhoneNumber.DigitSpellings[c];
            String spelling = ReadPhoneNumber.DigitSpellings.get(c);
            stringBuilder.append(String.format("%s ", spelling));
        }

        return stringBuilder.toString().trim();
    }
}

class ManageMyIO
{
	FileReader _fileReader;
	BufferedReader _buffReader;

	FileWriter _fileWriter;
	BufferedWriter _buffWriter;

	Boolean _isRWFromFile = false;

	public ManageMyIO()
	{
		try
		{

			if (_isRWFromFile)
			{
				_fileReader = new FileReader("Input.in");
				_buffReader = new BufferedReader(_fileReader);
				File outputFile = new File("Output.out");
				if (outputFile.exists() == true)
					outputFile.delete();
				outputFile.createNewFile();

				_fileWriter = new FileWriter(outputFile.getAbsoluteFile());
				_buffWriter = new BufferedWriter(_fileWriter);
			}
			else
			{
				_buffReader = new BufferedReader(new InputStreamReader(System.in));
				_buffWriter = new BufferedWriter(new OutputStreamWriter(System.out));
			}

		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public void write(String result) throws IOException
	{
		_buffWriter.write(result);
		_buffWriter.flush();
		
	}

	public String Read()
	{

		String ret = null;
		try

		{
			ret = _buffReader.readLine();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			return ret;
		}
	}

	public void write(Integer[] arr) throws Exception
	{
		String str = Arrays.toString(arr);
		str = str.replace("[", "").replace("]", "").replace(",", "") + "\n";
		_buffWriter.write(str);
		_buffWriter.flush();
	}
}
