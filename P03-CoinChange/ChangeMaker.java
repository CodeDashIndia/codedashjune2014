

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

public class ChangeMaker {
	
	private int[] denominations;
	
	public ChangeMaker(int[] denominations) {
		setDenominations(denominations);
	}
	
	public void setDenominations(int[] denominations) {
		Arrays.sort(denominations);
		ArrayList<Integer> validDenominations = new ArrayList<Integer>();
		for(int i : denominations) {
			if(i>0) {
				validDenominations.add(0,new Integer(i));
			}
		}
		this.denominations = new int[validDenominations.size()];
		for(int i=0;i<this.denominations.length;i++) {
			this.denominations[i] = validDenominations.get(i);
		}
	}
	
	public int makeMinChange(int amount) {
		int minCoinCount = 0;
		for(int biggestDenomIndex = 0;biggestDenomIndex < denominations.length; biggestDenomIndex++) {
			int[] counts = new int[denominations.length];
			Arrays.fill(counts, 0);
			boolean possible = method(counts, biggestDenomIndex, amount);
			if(possible) {
				int totalCoinCount = 0;
				for(int i=0;i<counts.length;i++) {
					if(counts[i] > 0) {
						totalCoinCount += counts[i];
					}
				}
				if(minCoinCount == 0 || totalCoinCount < minCoinCount) {
					minCoinCount = totalCoinCount;
				}
			}
		}
		return minCoinCount;
	}
	
	public boolean method(int[] counts,int currentDenomIndex,int amount) {
		if(amount == 0)
			return true;
		if(currentDenomIndex >= denominations.length) {
			return false;
		}
		int maxDiv = amount/denominations[currentDenomIndex];
		boolean possible = false;
		while(true) {
			counts[currentDenomIndex] = maxDiv;
			possible = method(counts, currentDenomIndex + 1, amount - (maxDiv * denominations[currentDenomIndex]));
			if(possible || maxDiv <= 0)
				break;
			maxDiv--;
		}
		
		return possible;
	}
	
	public static void main(String[] args) throws IOException {
		CodeJamIOProcessor iop = new CodeJamIOProcessor(System.in, System.out);

		int tcs = iop.getTestCases();
		
		for (int tc = 1; tc <= tcs; tc++) {
			String denomInputLine = iop.input();
			String amtInputLine = iop.input();
			
			StringTokenizer st = new StringTokenizer(denomInputLine, " ");
			int[] denoms = new int[st.countTokens()];
			int dIdx = 0;
			while(st.hasMoreTokens()) {
				denoms[dIdx++] = Integer.parseInt(st.nextToken());
			}
			
			ChangeMaker cm = new ChangeMaker(denoms);
			StringBuilder sb = new StringBuilder();
			
			st = new StringTokenizer(amtInputLine, " ");
			while(st.hasMoreElements()) {
				int cc = cm.makeMinChange(Integer.parseInt(st.nextToken()));
				sb.append(cc + " ");
			}
			
			iop.output(tc, sb.toString());
		}
	}
	
	static class CodeJamIOProcessor {

		private BufferedReader br;
		private BufferedWriter bw;
		private int testCases = 0;
		private InputStream input;

		public CodeJamIOProcessor(InputStream input, OutputStream output) {
			this.input = input;
			bw = new BufferedWriter(new OutputStreamWriter(output));
		}

		public int getTestCases() {
			if (br == null) {
				br = new BufferedReader(new InputStreamReader(input));
				try {
					String firstLine = br.readLine();
					if (firstLine != null) {
						testCases = Integer.parseInt(firstLine);
					}
				} catch (IOException e) {
					testCases = -1;
				}
			}
			return testCases;
		}
		
		public synchronized String input() throws IOException {
			if(br == null) {
				throw new RuntimeException("Uninitialized/Terminated input, try calling getTestCases() first"); 
			}
			String nextLine = br.readLine();
			if(nextLine == null) {
				br.close();
				br = null;
			}
			return nextLine;
		}
		
		public synchronized void output(int testCase,String line,boolean isComplete,boolean prependCaseNo) throws IOException {
			if(bw!=null) {
				bw.write((prependCaseNo ? ("Case #" + testCase + ": ") : "") + line + (prependCaseNo ? "\n" : ""));
				if(isComplete) {
					bw.flush();
					bw.close();
				}
			}
		}
		
		public synchronized void output(int testCase,String line,boolean prependCaseNo) throws IOException {
			output(testCase, line, this.testCases == testCase,prependCaseNo);
		}
		
		public synchronized void output(int testCase,String line) throws IOException {
			output(testCase, line, true);
		}
	}

}
