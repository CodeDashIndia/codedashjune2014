import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class ReorgNeeded
{

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		// TODO Auto-generated method stub

		ReOrgSolver reOrgSolver = new ReOrgSolver();
		reOrgSolver.Solve();
	}

}

enum Team
{
	None, A, B
}

class ReOrgSolver
{
	ManageMyIO ioManager;

	private int _testCaseCount = 0;

	public ReOrgSolver()
	{
		ioManager = new ManageMyIO();
	}

	public void Solve() throws IOException
	{
		String solutionStr;
		_testCaseCount = Integer.parseInt(ioManager.Read());
		for (int i = 0; i < _testCaseCount; i++)
		{
			TestCaseBadHorse testCaseBadHorse = TestCaseBadHorse.ReadAndCreate(ioManager);
			Boolean isPossible = testCaseBadHorse.Solve();
			solutionStr = "Case #" + (i+1) + ": " + (isPossible ? "Yes" : "No") + "\n";
			ioManager.write(solutionStr);
		}
	}
}

class TestCaseBadHorse
{
	public Hashtable<String, BanditWithIssues> _bandits = new Hashtable<String, BanditWithIssues>();
	
	public static TestCaseBadHorse ReadAndCreate(ManageMyIO reader)
    {
        TestCaseBadHorse testCase = new TestCaseBadHorse();
        int pairCount = Integer.parseInt(reader.Read());

        for (int j = 0; j < pairCount; j++)
        {
            String line = reader.Read();
            String[] names = line.split(" ");
            testCase.ProcessCoupleWithIssues(names);
        }

        return testCase;
    }

	public Boolean Solve()
    {
        //foreach (KeyValuePair<String, BanditWithIssues> keyValuePair in _bandits)
        //{
        //    BanditWithIssues bandit = keyValuePair.Value;
        //    if (CheckForDeadlockEnemity(bandit) == false)
        //        return false;
        //}

		String key1 =_bandits.keys().nextElement();
		BanditWithIssues bandit1 = _bandits.get(key1);;
        bandit1.Team = Team.A;
        return CheckRecursivelyForTeamConflicts(bandit1);
    }

    private Boolean CheckRecursivelyForTeamConflicts(BanditWithIssues bandit1)
    {
        for (String enemyName : bandit1.IssuesList)
        {
            BanditWithIssues enemyBandit = _bandits.get(enemyName);
            if (enemyBandit.Team == bandit1.Team)
            {
                return false;
            }

            if (enemyBandit.Team == Team.None)
            {
                enemyBandit.Team = (bandit1.Team == Team.A) ? Team.B : Team.A;

                if (CheckRecursivelyForTeamConflicts(enemyBandit) == false)
                    return false;
            }
        }

        return true;
    }

    private Boolean CheckForDeadlockEnemity(BanditWithIssues bandit)
    {
        int count = bandit.IssuesList.size();

        for (int i = 0; i < count - 1; i++)
        {
            BanditWithIssues enemyBandit = _bandits.get(bandit.IssuesList.get(i));
            for (int j = i + 1; j < count; j++)
            {
                if (enemyBandit.IssuesList.contains(bandit.IssuesList.get(j)))
                {
                    return false;
                }
            }
        }

        return true;
    }

    private void ProcessCoupleWithIssues(String[] names)
    {
        String Name1 = names[0];
        String Name2 = names[1];

        if (_bandits.containsKey(Name1) == false)
        {
            _bandits.put(Name1, new BanditWithIssues(Name1));
        }
        if (_bandits.containsKey(Name2) == false)
        {
            _bandits.put(Name2, new BanditWithIssues(Name2));
        }

        BanditWithIssues bandit1 = _bandits.get(Name1);
        BanditWithIssues bandit2 = _bandits.get(Name2);
        bandit1.IssuesList.add(bandit2.Name);
        bandit2.IssuesList.add(bandit1.Name);
    }
	
}

class BanditWithIssues
{
    public BanditWithIssues(String name)
    {
        Name = name;
        Team = Team.None;
    }

    public Team Team;
    public String Name;

    public List<String> IssuesList = new ArrayList<String>();
}

class ManageMyIO
{
	FileReader _fileReader;
	BufferedReader _buffReader;

	FileWriter _fileWriter;
	BufferedWriter _buffWriter;

	Boolean _isRWFromFile = false;

	public ManageMyIO()
	{
		try
		{

			if (_isRWFromFile)
			{
				_fileReader = new FileReader("../Input.in");
				_buffReader = new BufferedReader(_fileReader);
				File outputFile = new File("Output.out");
				if (outputFile.exists() == true)
					outputFile.delete();
				outputFile.createNewFile();

				_fileWriter = new FileWriter(outputFile.getAbsoluteFile());
				_buffWriter = new BufferedWriter(_fileWriter);
			}
			else
			{
				_buffReader = new BufferedReader(new InputStreamReader(
						System.in));
				_buffWriter = new BufferedWriter(new OutputStreamWriter(
						System.out));
			}

		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public void write(String result) throws IOException
	{
		_buffWriter.write(result);
		_buffWriter.flush();

	}

	public String Read()
	{

		String ret = null;
		try

		{
			ret = _buffReader.readLine();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			return ret;
		}
	}

	public void write(Integer[] arr) throws Exception
	{
		String str = Arrays.toString(arr);
		str = str.replace("[", "").replace("]", "").replace(",", "") + "\n";
		_buffWriter.write(str);
		_buffWriter.flush();
	}
}