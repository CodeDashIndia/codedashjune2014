﻿Reorganization Needed:

We have a large team of software engineers. Some of them have a major problem with each other. They just cannot exist in the same team together. So we are thinking of splitting the team into two smaller groups IF possible. A group cannot have any engineer who has a conflict with any other engineer in that group. Minimum group size is one engineer. You need to help us find out if that is possible


Input: 
The first line contains a positive integer N. N test cases follow. 
Each test case starts with a positive integer M. The next M lines contain a pair of names separated by a space. Each such pair represents the names of two engineers who have a problem with each other.


Output: For each test case output one line containing "Case #x: y" where x is the test case number starting from 1. 
y is either “Yes” or “No” depending on whether the group can be divided into two smaller groups.

Limits:
1 ≤ N ≤ 100.

Example

Input:
2
3
Axl_Rose Dave_Mustaine
Dave_Mustaine James_Hetfield
James_Hetfield Axl_Rose
2
Axl_Rose Dave_Mustaine
James_Hetfield Axl_Rose

Output :
Case #1: No
Case #2: Yes
